import os.path
import re
import socket
import ssl
import base64
import magic
from random import randint

from typing import Union

from config import HOST_ADDRESS, PORT
from auth import login, password


def request(socket: socket.socket, request: str) -> str:
    socket.send((request + '\n').encode())
    return socket.recv(1024).decode()


def parse_params(filename: str) -> dict[str, Union[str, list]]:
    result = {
        "RECEIVERS": [],
        "SUBJECT": "",
        "BODY": "",
        "ATTACHMENTS": []
    }
    with open(filename, "r", encoding='utf-8') as f:
        current_field = ""
        for line in f.readlines():
            if line[:-1] in result.keys():
                current_field = line[:-1]
                continue
            if current_field == 'SUBJECT':
                result[current_field] += line[:-1]
            elif current_field == 'BODY':
                result[current_field] += line
            else:
                result[current_field].append(line[:-1])
    return result


def handle_subject(subject: str) -> str:
    if not re.search(r'[а-яА-ЯёЁ]', subject):
        return subject

    parts = []
    for i in range(0, len(subject), 30):
        part = subject[i:min(len(subject), i + 30)]
        base64part = base64.b64encode(part.encode()).decode()
        parts.append(f'=?utf-8?B?{base64part}?=')

    return "Subject: " + '\n\t'.join(parts) + '\n'


def generate_boundary() -> str:
    return f"bound.{''.join([chr(randint(97, 123)) for _ in range(5)])}"


def generate_message(params: dict[str, Union[str, list]]) -> str:
    message_parts = []
    boundary = generate_boundary()

    message_parts.append(f'From: <{login}@yandex.ru>\n')
    message_parts.append(f'To: ' + ',\n\t'.join(map(lambda s: f'<{s}>', params['RECEIVERS'])) + '\n')
    message_parts.append(handle_subject(params['SUBJECT']))
    message_parts.append('MIME-Version: 1.0\n')
    message_parts.append(f"Content-Type: multipart/mixed;boundary=\"{boundary}\"\n")
    message_parts.append("\n\n")

    message_parts.append(f"--{boundary}\n")
    message_parts.append("Content-Type: text/html\n\n")

    for line in params["BODY"].split("\n"):
        if re.match("^\.+$", line):
            line += "."
        message_parts.append(f'{line}\n')

    for filename in params['ATTACHMENTS']:
        if not os.path.isfile(f'attachments/{filename}'):
            print(f"Файла {f'{filename}'} не существует")
            continue
        message_parts.append(f"--{boundary}\n")
        message_parts.append("Content-Disposition: attachment;\n")
        message_parts.append(f"\tfilename=\"{filename}\"\n")
        message_parts.append("Content-Transfer-Encoding: base64\n")

        mime_type = magic.from_file(f"attachments/{filename}", mime=True)
        message_parts.append(f"Content-Type: {mime_type};\n")

        message_parts.append(f"\tname=\"{filename}\"\n\n")

        with open(f"attachments/{filename}", "rb") as file:
            image = base64.b64encode(file.read()).decode("utf-8")
            message_parts.append(image + '\n')

    message_parts.append(f"--{boundary}--")
    message_parts.append("\n.\n")

    return ''.join(message_parts)


def send_mail(params: dict[str, Union[str, list]]) -> None:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        client.connect((HOST_ADDRESS, PORT))
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        client = context.wrap_socket(client)

        client.recv(1024)
        request(client, f"EHLO {login}")

        base64login = base64.b64encode(login.encode()).decode()
        base64password = base64.b64encode(password.encode()).decode()

        request(client, 'AUTH LOGIN')
        response = request(client, base64login)
        response = request(client, base64password)
        if not response.startswith("235"):
            return print("Ошибка аутентификации")

        response = request(client, f'MAIL FROM:{login}@yandex.ru')
        if not response.startswith("250"):
            return print("Неправильный отправитель")

        for receiver in params["RECEIVERS"]:
            response = request(client, f"RCPT TO:{receiver}")
            if not response.startswith("250"):
                return print(f"Неправильный получатель {receiver}")

        request(client, 'DATA')
        request(client, generate_message(params))


if __name__ == "__main__":
    send_mail(parse_params("mail.txt"))
